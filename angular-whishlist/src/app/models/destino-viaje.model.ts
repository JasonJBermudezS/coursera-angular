import { ThrowStmt } from '@angular/compiler';

export class DestinoViaje {
  private selected: boolean;
  public servicios: string[];

  constructor(public nombre: string, public u: string) {
    this.servicios = ['Lavandería', 'Desayuno'];
  }
  isSelected(): boolean {
    return this.selected;
  }

  setSelected(s: boolean) {
    this.selected = s;
  }
}
